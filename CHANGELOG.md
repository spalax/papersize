* papersize 1.6.0 (2024-12-27)

    * Add Python3.13 support.

    -- Louis Paternault <spalax@gresille.org>

* papersize 1.5.0 (2024-02-18)

    * Drop python3.7 support.
    * Use Hatch as a build system.
    * Add internationalisation support
        * Add French translations

    -- Louis Paternault <spalax@gresille.org>

* papersize 1.4.0 (2023-10-07)

    * Python3.12 support.

    -- Louis Paternault <spalax@gresille.org>

* papersize 1.3.0 (2022-11-29)

    * Python3.11 support.

    -- Louis Paternault <spalax@gresille.org>

* papersize 1.2.0 (2021-09-05)

    * Add python3.10 support
    * Add variables `UNITS_HELP` and `SIZES_HELP`.

    -- Louis Paternault <spalax@gresille.org>

* papersize 1.1.0 (2021-03-07)

    * Python support
        * Drop python2, python3.4 to python3.6 support.
        * Add python3.7 to python3.9 support.
    * Features
        * Add `strict` and `fuzzy` comparison.
        * Use more precise conversion value between length units.
    * Documentation
        * Add list of known units and paper sizes.
        * Minor improvements.
    * Improve setup.

    -- Louis Paternault <spalax@gresille.org>

* papersize 1.0.1 (2017-12-09)

    * Fix README formatting.

    -- Louis Paternault <spalax@gresille.org>

* papersize 1.0.0 (2017-12-08)

    * Add python3.6 support.

    -- Louis Paternault <spalax@gresille.org>

* papersize 0.1.3 (2015-06-13)

    * Python3.5 support
    * Several minor improvements to setup, test and documentation.

    -- Louis Paternault <spalax@gresille.org>

* papersize 0.1.2 (2015-03-14)

    * New version to solve packaging issues.

    -- Louis Paternault <spalax@gresille.org>

* papersize 0.1.1 (2015-03-14)

    * Bugs
        * Closes bug: Paper size was not case sensitive
    * Meta
        * Changed project URL to http://git.framasoft.org/spalax/papersize
    * Internal
        * Internal change: module is no longer imported to run setup.py or build
          documentation.
        * Added tests

    -- Louis Paternault <spalax@gresille.org>

* papersize 0.1.0 (2015-01-24)

    * First published version.

    -- Louis Paternault <spalax@gresille.org>
